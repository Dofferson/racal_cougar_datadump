
/*
 * WARNING
 * 
 * Pin F of the Racal Cougar E.C.U / Audio connector communicates using PWM data of about
 * 8 Volt. Since the Arduino can't handle this voltage level some kind of level shifting to 
 * 5 volt is required (i.e. a resistor voltage divider) is required.
 * 
 */

// Pin number connected to the 'F' Pin of the Racal Cougar data cable
// this should be a pin which supports an interrupt. (pins 2 and 3 on an Nano)
#define FPin 3

/*
 * The Volatile qualifier instructs the compiler to
 * "store" these variables in RAM instead of storage registers
 * The reason for this is that the storage rigisters can be inaccurate when
 * accessing them from within an interrupt service routine
 */
volatile int pwm_value = 0;             // the 'width' of a PWM pulse in uSec
volatile unsigned long prev_time = 0;   // Helper to determin the pwm value
volatile int bitCounter = 8;            // Bit number comming in
volatile byte myByte = 0;               // The byte that is getting filled with incomming bits
volatile byte data[7];                  // Array of bytes which will be displayed after the last bit has come in.
volatile int byteIndex = 0;             // Index of the byte being stored in the data array

/**
 * Setup routine
 */
void setup() {

  Serial.begin(250000);
  pinMode(FPin, INPUT_PULLUP);
  
  // Start with attaching a falling signal (+ to 0) event to
  // trigger an incomming bit 
  attachInterrupt(1, falling, FALLING);
  
}

/**
 * Main loop
 * 
 * Wait untill there is no change for 1mS and dump the contents
 * of the data array to the serial port.
 */
void loop() {

  // Contains the current running time in uSec
  unsigned long mTime = micros();

  // When there has been no change for 1 mSec
  if (prev_time > 0 && prev_time < (mTime - 1000)) {

      // for incomplete bytes (i.e. a 52 bit stream)
      // add it to the data array
      if (bitCounter < 8) {
        bitCounter = 8;
        data[byteIndex] = myByte;
        byteIndex++;
      }
      
      // Loop over the data byte array and print the binary value of the data byte.
      // Also print the Hex value.
      for (int lusa=0; lusa < byteIndex; lusa++) {
       
        for (byte mask = 0x80; mask; mask >>= 1) {
           Serial.print(mask & data[lusa] ? '1':'0');
        }
        Serial.print(' ');
        Serial.println(data[lusa], HEX);
        
      }
      
      Serial.println("-------");

      // reset counters etc.
      prev_time = 0;
      byteIndex = 0;
      memset(data, 0, sizeof(data));
      myByte = 0;
  }
}

/**
 * Falling ISR 
 * 
 * This is the start of a pulse event, stores the current running time
 * and changes the interrupt from a falling to a rising event
 */
void falling() {
  prev_time = micros();
  attachInterrupt(1, rising, RISING);
}

/**
 * Rising ISR
 * 
 * This is the end of a pulse event, it measures the time between falling and rising
 * to determine the bit value being send. 
 */
void rising() {

  // reset the interrupt to a falling state
  attachInterrupt(1, falling, FALLING);
   
  // Contains the current running time in uSec
  unsigned long mTime = micros();
 
  
  // calculate the pulse width
  pwm_value = mTime - prev_time;

  prev_time = mTime;
  
  // Set the bit value into the byte
  // 0 = 75 uSec
  // 1 = 175 uSec
  if( pwm_value > 100) {
    myByte =   myByte | (1 << (bitCounter -1)) ;
  }

  // lower the bitcounter and store the byte in the data array when the last
  // bit of the byte has been set.
  bitCounter--;
  if (bitCounter == 0) {
    bitCounter = 8;
    data[byteIndex] = myByte;
    byteIndex++;
    myByte = 0;
  }
}

